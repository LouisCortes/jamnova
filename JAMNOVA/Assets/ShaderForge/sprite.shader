// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-1710-RGB,clip-1710-A;n:type:ShaderForge.SFN_Tex2d,id:1710,x:32115,y:32788,ptovrint:False,ptlb:sprite,ptin:_sprite,varname:node_1710,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5bb6fa24212f94f4baae7aa6488df4ca,ntxv:0,isnm:False|UVIN-6928-UVOUT;n:type:ShaderForge.SFN_UVTile,id:6928,x:32049,y:32625,varname:node_6928,prsc:2|UVIN-6978-UVOUT,WDT-4784-OUT,HGT-4784-OUT,TILE-8967-OUT;n:type:ShaderForge.SFN_Vector1,id:4784,x:31811,y:32644,varname:node_4784,prsc:2,v1:6;n:type:ShaderForge.SFN_Slider,id:2811,x:30982,y:32618,ptovrint:False,ptlb:animation,ptin:_animation,varname:node_2811,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Multiply,id:8535,x:31554,y:32814,varname:node_8535,prsc:2|A-1235-OUT,B-5957-OUT,C-2811-OUT;n:type:ShaderForge.SFN_Vector1,id:5957,x:31484,y:32983,varname:node_5957,prsc:2,v1:31;n:type:ShaderForge.SFN_Floor,id:8967,x:31766,y:32733,varname:node_8967,prsc:2|IN-8535-OUT;n:type:ShaderForge.SFN_TexCoord,id:6978,x:31460,y:32513,varname:node_6978,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Frac,id:1235,x:31320,y:32923,varname:node_1235,prsc:2|IN-4885-OUT;n:type:ShaderForge.SFN_Time,id:394,x:31048,y:32875,varname:node_394,prsc:2;n:type:ShaderForge.SFN_Slider,id:5876,x:30945,y:32749,ptovrint:False,ptlb:speed,ptin:_speed,varname:node_5876,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Multiply,id:4885,x:31265,y:32749,varname:node_4885,prsc:2|A-5876-OUT,B-394-T;proporder:1710-2811-5876;pass:END;sub:END;*/

Shader "Shader Forge/sprite" {
    Properties {
        _sprite ("sprite", 2D) = "white" {}
        _animation ("animation", Range(0, 1)) = 0
        _speed ("speed", Range(0, 10)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _sprite; uniform float4 _sprite_ST;
            uniform float _animation;
            uniform float _speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_4784 = 6.0;
                float4 node_394 = _Time;
                float node_8535 = (frac((_speed*node_394.g))*31.0*_animation);
                float node_8967 = floor(node_8535);
                float2 node_6928_tc_rcp = float2(1.0,1.0)/float2( node_4784, node_4784 );
                float node_6928_ty = floor(node_8967 * node_6928_tc_rcp.x);
                float node_6928_tx = node_8967 - node_4784 * node_6928_ty;
                float2 node_6928 = (i.uv0 + float2(node_6928_tx, node_6928_ty)) * node_6928_tc_rcp;
                float4 _sprite_var = tex2D(_sprite,TRANSFORM_TEX(node_6928, _sprite));
                clip(_sprite_var.a - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _sprite_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _sprite; uniform float4 _sprite_ST;
            uniform float _animation;
            uniform float _speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float node_4784 = 6.0;
                float4 node_394 = _Time;
                float node_8535 = (frac((_speed*node_394.g))*31.0*_animation);
                float node_8967 = floor(node_8535);
                float2 node_6928_tc_rcp = float2(1.0,1.0)/float2( node_4784, node_4784 );
                float node_6928_ty = floor(node_8967 * node_6928_tc_rcp.x);
                float node_6928_tx = node_8967 - node_4784 * node_6928_ty;
                float2 node_6928 = (i.uv0 + float2(node_6928_tx, node_6928_ty)) * node_6928_tc_rcp;
                float4 _sprite_var = tex2D(_sprite,TRANSFORM_TEX(node_6928, _sprite));
                clip(_sprite_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
