﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perso : MonoBehaviour {

    public ConditionScriptG2 Condition;

    public bool LostLife;
    public DetectionCubeCollision ColCube;
    public GameObject ScreenJ2Win;
    public GameObject Go;
    public int LifeBlue;
    public GameObject PersoB;

    public  float UnitDeplacement;
    private float Xpos;
    private float Ypos;
    private float Zpos;
    // Use this for initialization
    void Start () {
        LostLife = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.RightArrow) && Go.transform.position.x<1)
        {
            Go.transform.Translate(UnitDeplacement, Ypos, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && Go.transform.position.x > -1)
        {
            Go.transform.Translate(-UnitDeplacement, Ypos, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && Go.transform.position.z > -1)
        {
            Go.transform.Translate(Xpos, -UnitDeplacement, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && Go.transform.position.z < 1)
        {
            Go.transform.Translate(Xpos, UnitDeplacement, Zpos);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Blue")
        {
            Debug.Log("BLUE CUBE DETECTED");
            LostOneLife();
        }
    }

    void LostOneLife()
    {
        LifeBlue--;
        LostLife = true;
        if (LifeBlue == 3)
        {

        }
        
        if (LifeBlue == 2)
        {
            PersoB.transform.localScale = new Vector3(1F,1, 1);
        }
        if (LifeBlue == 1)
        {
            PersoB.transform.localScale = new Vector3(0.5f , 0.5f, 0.5f);
        }
        if (LifeBlue == 0)
        {
            PersoB.SetActive(false);
            Condition.RedWin();
        }

       // ColCube.ResetPositionCube();
        
    }
}
