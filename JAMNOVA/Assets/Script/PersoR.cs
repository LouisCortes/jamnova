﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersoR : MonoBehaviour
{
    public ConditionScriptG2 Condition;

    public bool LostLifeR;
    public DetectionCubeCollision ColCube;
    public GameObject ScreenJ1Win;
    public GameObject Go;
    public int LifeRed;
    public GameObject PersoRed;

    public float UnitDeplacement;
    private float Xpos;
    private float Ypos;
    private float Zpos;
    // Use this for initialization
    void Start()
    {
        LostLifeR = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.D) && Go.transform.position.x < 29)
        {
            Go.transform.Translate(UnitDeplacement, Ypos, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.Q) && Go.transform.position.x > 27)
        {
            Go.transform.Translate(-UnitDeplacement, Ypos, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.S) && Go.transform.position.z > -1)
        {
            Go.transform.Translate(Xpos, -UnitDeplacement, Zpos);
        }
        if (Input.GetKeyDown(KeyCode.Z) && Go.transform.position.z < 1)
        {
            Go.transform.Translate(Xpos, UnitDeplacement, Zpos);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Red")
        {
            LostOneLifeR();
        }
    }

    void LostOneLifeR()
    {
        LifeRed--;
        LostLifeR = true;

        if (LifeRed == 2)
        {
            PersoRed.transform.localScale = new Vector3(1F, 1, 1);
        }
        if (LifeRed == 1)
        {
            PersoRed.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        if (LifeRed == 0)
        {
            PersoRed.SetActive(false);
            Condition.BlueWin();
        }

    }
}
