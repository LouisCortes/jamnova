﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionScript : MonoBehaviour
{

    public GameObject ControlInfo;

    public Animator AC_Win;
    public GameObject goBlueWin;
    public Animator AC_BlueWin;

    public GameObject goRedWin;
    public Animator AC_RedWin;
    // Start is called before the first frame update
    void Start()
    {
        goRedWin.SetActive(false);
        goBlueWin.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.M))
        {
            Application.LoadLevel("SCN_Menu");
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Application.LoadLevel("SCN_Game01");
        }
    }
    public void BlueWin()
    {
        goBlueWin.SetActive(true);
        AC_Win.Play("AN_BlinkBlue");

    }

    public void RedWin()
    {
        goRedWin.SetActive(true);
        AC_Win.Play("AN_BlinkRed");
    }

    public void Reload()
    {
        Application.LoadLevel("SCN_Game01");
    }

}
