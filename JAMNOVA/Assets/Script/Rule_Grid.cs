﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rule_Grid : MonoBehaviour
{
    public ConditionScript S_Condition;


    //public bool 
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Blue")
        {
            Debug.Log("BLUE WIN");
            S_Condition.BlueWin();
        }
        else if (collision.gameObject.tag == "Red")
        {
            Debug.Log("RED WIN");
            S_Condition.RedWin();
        }
    }
}
