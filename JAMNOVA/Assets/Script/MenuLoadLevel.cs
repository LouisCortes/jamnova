﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLoadLevel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Application.LoadLevel("SCN_Game01");
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Application.LoadLevel("SCN_Game02");
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Application.LoadLevel("PlanNova2Start");
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Application.LoadLevel("Credits");
        }
    }
}
