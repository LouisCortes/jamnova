﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Blue2 : MonoBehaviour
{
    public GameObject B_CubeTop1;
    public GameObject B_CubeTop2;
    public GameObject B_CubeTop3;
    // public GameObject B_CubeTop4;
    // public GameObject B_CubeTop5;
    public GameObject B_CubeLeft1;
    public GameObject B_CubeLeft2;
    public GameObject B_CubeLeft3;

    public GameObject B_CubeRight1;
    public GameObject B_CubeRight2;
    public GameObject B_CubeRight3;

    public GameObject B_CubeDown1;
    public GameObject B_CubeDown2;
    public GameObject B_CubeDown3;

    public float Speed;
    public float TimeGlobal;
    public float Counter01;
    public float Counter02;
    public float Counter03;
    public float Duration;
    public float Duration2;
    public float Duration3;
    public int RandomNum;
    public int RandomNum2;
    public int RandomNum3;

    public float UnitDeplacement;
    //public GameObject Blue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Counter01 += (Duration/25)  * Time.deltaTime;
        Counter02 += (Duration2/25) * Time.deltaTime;
        Counter03 += (Duration3/25) * Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.A))
        {
            
        }

        if(Counter01 > Duration)
        {
            Counter01 = 0;
            RandomNum = Random.Range(0, 4);
        }
        if (Counter02 > Duration2)
        {
            Counter02 = 0;
            RandomNum2 = Random.Range(0, 4);
        }
        if (Counter03 > Duration3)
        {
            Counter03 = 0;
            RandomNum3 = Random.Range(0, 4);
        }

        if ( RandomNum == 1)
        {
            B_CubeTop1.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum == 2)
        {
            B_CubeTop2.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum == 3)
        {
            B_CubeTop3.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }
        /*  if (RandomNum == 4)
          {
              B_CubeTop4.transform.Translate(0, 0, (-1 * Speed) * Time.deltaTime);
          }
          if (RandomNum == 5)
          {
              B_CubeTop5.transform.Translate(0, 0, (-1 * Speed) * Time.deltaTime);
          }
          */
        if (RandomNum3 == 1)
        {
            B_CubeLeft1.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum3 == 2)
        {
            B_CubeLeft2.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum3 == 3)
        {
            B_CubeLeft3.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }

        if (RandomNum == 1)
        {
            B_CubeRight1.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum == 2)
        {
            B_CubeRight2.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum == 3)
        {
            B_CubeRight3.transform.Translate(0, 0, (-UnitDeplacement * Speed) * Time.deltaTime);
        }

        if (RandomNum2 == 1)
        {
            B_CubeDown1.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum2 == 2)
        {
            B_CubeDown2.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }
        if (RandomNum2 == 3)
        {
            B_CubeDown3.transform.Translate(0, 0, (UnitDeplacement * Speed) * Time.deltaTime);
        }

    }




}
