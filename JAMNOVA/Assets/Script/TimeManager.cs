﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public Perso S_Perso;
    public PersoR S_PersoRed;
    public float SpeedValue;
    public int turn;
    public int RandomNum;
    public int RandomNumR;
    public bool StartTime;
    public Animator AC_Time;
    // Start is called before the first frame update
    void Start()
    {
        SpeedValue = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTime == true)
        {
            AC_Time.Play("AN_Timer");
        }
    }

    public void GenerateRandom()
    {
        S_Perso.LostLife = false;
        S_PersoRed.LostLifeR = false;
        RandomNum = Random.Range(1, 7);
        RandomNumR = Random.Range(1, 7);
        SpeedValue += 0.05f;
        AC_Time.SetFloat("Speed", SpeedValue);
    }
}
