﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_BlueAnim : MonoBehaviour
{
    public Perso S_Perso;
    public Animator ACTop;
    public Animator ACDown;
    public Animator ACRight;
    public Animator ACLeft;

    public GameObject B_CubeTop1;
    public GameObject B_CubeTop2;
    public GameObject B_CubeTop3;
    // public GameObject B_CubeTop4;
    // public GameObject B_CubeTop5;
    public GameObject B_CubeLeft1;
    public GameObject B_CubeLeft2;
    public GameObject B_CubeLeft3;

    public GameObject B_CubeRight1;
    public GameObject B_CubeRight2;
    public GameObject B_CubeRight3;

    public GameObject B_CubeDown1;
    public GameObject B_CubeDown2;
    public GameObject B_CubeDown3;

    public float Speed;
    public float TimeGlobal;
    public float Counter01;
    public float Counter02;
    public float Counter03;
    public float Duration;
    public float Duration2;
    public float Duration3;
    public TimeManager S_Time;
    //public int RandomNum;
    public int RandomNum2;
    public int RandomNum3;

    public float UnitDeplacement;
    //public GameObject Blue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ACTop.SetFloat("SpeedBTop", S_Time.SpeedValue);
        ACDown.SetFloat("SpeedBDown", S_Time.SpeedValue);
        ACRight.SetFloat("SpeedBRight", S_Time.SpeedValue);
        ACLeft.SetFloat("SpeedBLeft", S_Time.SpeedValue);

        Counter01 += (Duration/5)  * Time.deltaTime;
        Counter02 += (Duration2/25) * Time.deltaTime;
        Counter03 += (Duration3/25) * Time.deltaTime;

        if (S_Perso.LostLife == true)
        {
            S_Time.RandomNum = 0;
            ACTop.Play("AN_BTopCubeInit");
            ACDown.Play("AN_BDownCubeInit");
            ACRight.Play("AN_BRightCubeInit");
            ACLeft.Play("AN_BLeftCubeInit");
           // S_Perso.LostLife = false;
        }

        if (S_Time.RandomNum == 1)
        {
            ACTop.Play("AN_BTopCube1");
            ACDown.Play("AN_BDownCube1");
            ACRight.Play("AN_BRightCube1");
            ACLeft.Play("AN_BLeftCube1");
        }else if (S_Time.RandomNum == 2)
        {

            ACTop.Play("AN_BTopCube2");
            ACDown.Play("AN_BDownCube2");
            ACRight.Play("AN_BRightCube2");
            ACLeft.Play("AN_BLeftCube2");
        }else if (S_Time.RandomNum == 3)
        {

            ACTop.Play("AN_BTopCube3");
            ACDown.Play("AN_BDownCube3");
            ACRight.Play("AN_BRightCube3");
            ACLeft.Play("AN_BLeftCube3");
        }else if (S_Time.RandomNum == 4)
        {

            ACTop.Play("AN_BTopCube1");
            ACDown.Play("AN_BDownCube2");
            ACRight.Play("AN_BRightCube2");
            ACLeft.Play("AN_BLeftCube3");
        }
        else if (S_Time.RandomNum == 5)
        {

            ACTop.Play("AN_BTopCube3");
            ACDown.Play("AN_BDownCube2");
            ACRight.Play("AN_BRightCube3");
            ACLeft.Play("AN_BLeftCube2");
        }else if (S_Time.RandomNum == 6)
        {

            ACTop.Play("AN_BTopCube1");
            ACDown.Play("AN_BDownCube2");
            ACRight.Play("AN_BRightCube1");
            ACLeft.Play("AN_BLeftCube2");

        }else if (S_Time.RandomNum == 7)
        {

            ACTop.Play("AN_BTopCube2");
            ACDown.Play("AN_BDownCube2");
            ACRight.Play("AN_BRightCube3");
            ACLeft.Play("AN_BLeftCube1");
        }


    }

   




}
