﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_RedAnim : MonoBehaviour
{
    public PersoR S_PersoR;
    public Animator ACTopR;
    public Animator ACDownR;
    public Animator ACRightR;
    public Animator ACLeftR;

    public GameObject R_CubeTop1;
    public GameObject R_CubeTop2;
    public GameObject R_CubeTop3;
    // public GameObject B_CubeTop4;
    // public GameObject B_CubeTop5;
    public GameObject R_CubeLeft1;
    public GameObject R_CubeLeft2;
    public GameObject R_CubeLeft3;

    public GameObject R_CubeRight1;
    public GameObject R_CubeRight2;
    public GameObject R_CubeRight3;

    public GameObject R_CubeDown1;
    public GameObject R_CubeDown2;
    public GameObject R_CubeDown3;

    public float Speed;
    public float TimeGlobal;

    public TimeManager S_Time;
    //public int RandomNum;
    public int RandomNumR;


    public float UnitDeplacement;
    //public GameObject Blue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ACTopR.SetFloat("SpeedBTop", S_Time.SpeedValue);
        ACDownR.SetFloat("SpeedBDown", S_Time.SpeedValue);
        ACRightR.SetFloat("SpeedBRight", S_Time.SpeedValue);
        ACLeftR.SetFloat("SpeedBLeft", S_Time.SpeedValue);


        if (S_PersoR.LostLifeR == true)
        {
            S_Time.RandomNumR = 0;
            ACTopR.Play("AN_RTopCubeInit");
            ACDownR.Play("AN_RDownCubeInit");
            ACRightR.Play("AN_RRightCubeInit");
            ACLeftR.Play("AN_RLeftCubeInit");
            // S_Perso.LostLife = false;
        }

        if (S_Time.RandomNumR == 1)
        {
            ACTopR.Play("AN_RTopCube1");
            ACDownR.Play("AN_RDownCube1");
            ACRightR.Play("AN_RRightCube1");
            ACLeftR.Play("AN_RLeftCube1");
        }
        else if (S_Time.RandomNum == 2)
        {
            Debug.Log("Anim2");
            ACTopR.Play("AN_RTopCube2");
            ACDownR.Play("AN_RDownCube2");
            ACRightR.Play("AN_RRightCube2");
            ACLeftR.Play("AN_RLeftCube2");
        }
        else if (S_Time.RandomNumR == 3)
        {
            Debug.Log("Anim3");
            ACTopR.Play("AN_RTopCube3");
            ACDownR.Play("AN_RDownCube3");
            ACRightR.Play("AN_RRightCube3");
            ACLeftR.Play("AN_RLeftCube3");
        }
        else if (S_Time.RandomNum == 4)
        {
            Debug.Log("Anim4");
            ACTopR.Play("AN_RTopCube1");
            ACDownR.Play("AN_RDownCube2");
            ACRightR.Play("AN_RRightCube2");
            ACLeftR.Play("AN_RLeftCube3");
        }
        else if (S_Time.RandomNumR == 5)
        {

            ACTopR.Play("AN_RTopCube3");
            ACDownR.Play("AN_RDownCube2");
            ACRightR.Play("AN_RRightCube3");
            ACLeftR.Play("AN_RLeftCube2");
        }
        else if (S_Time.RandomNumR == 6)
        {
 
            ACTopR.Play("AN_RTopCube1");
            ACDownR.Play("AN_RDownCube2");
            ACRightR.Play("AN_RRightCube1");
            ACLeftR.Play("AN_RLeftCube3");

        }
        else if (S_Time.RandomNumR == 7)
        {
   
            ACTopR.Play("AN_RTopCube2");
            ACDownR.Play("AN_RDownCube2");
            ACRightR.Play("AN_RRightCube3");
            ACLeftR.Play("AN_RLeftCube1");
        }


    }






}
