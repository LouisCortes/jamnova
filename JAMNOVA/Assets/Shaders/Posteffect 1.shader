﻿Shader "Unlit/Posteffect1"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_f1 ("_f1",Range (0.,1.)) =1
		_f2 ("_f2",Range (0.,1.)) =1
		_f3 ("_f3",Range (0.,1.)) =1
		_f4 ("_f4",Range (0.,1.)) =1
		_f5 ("_f5",Range (0.,1.)) =1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _f1;
			float _f2;
			float _f3;
			float _f4;
			float _f5;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
              float prop = _ScreenParams.xy;
			 float2 uv = i.uv;
			float2 ndcPos = uv * 2.0 - 1.0;

			float aspect = _ScreenParams.x / _ScreenParams.y;
			float an = _f1*2.;
			float half_dist = tan(an);

			float2  vp_scale = float2(aspect*_f3*2., _f2*2.);
			float2  P = ndcPos * vp_scale;     
			float vp_dia   = length(vp_scale);
			float rel_dist = length(P) / vp_dia;  
			float2  rel_P = normalize(P) / normalize(vp_scale);
			float2 pos_prj = ndcPos;
   
				float beta = rel_dist *an;
				pos_prj = rel_P * tan(beta) / half_dist;  

			float2 uv_prj = pos_prj * 0.5 + 0.5;  
                return tex2D(_MainTex,uv_prj);
				
            }
            ENDCG
        }
    }
}
