﻿Shader "Unlit/fille"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_r1 ("_r1",Range (0.,1.)) =1
		_r2 ("_r2",Range (0.,1.)) =1.
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _r1;
			float _r2;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
			float hash (float t){return frac(sin(dot(floor(t),45.236))*4587.236);}
			float no (float t){ return lerp(hash(t),hash(t+1.),smoothstep(0.,1.,frac(t)));}
            fixed4 frag (v2f i) : SV_Target
            {
                
                fixed4 col = tex2D(_MainTex, i.uv);
                float2 nor = float2 ((col.y-0.5)*2.,(col.z-0.5)*2.);
				float li1 = smoothstep(_r1+no(_Time.x*400.+85.236)*0.2,_r2+no(_Time.x*400.+18.704)*0.2,dot(nor,float2((no(_Time.x*400.+47.236)-0.5)*2.,(no(_Time.x*400.+28.147)-0.5)*2.)));
				float3 li2 = lerp (float3(1.,0.,0.),float3(0.,0.,1.),hash(_Time.x*500.+61.444))*li1;
				float3 li3 = lerp(float3(1.,0.,0.),float3(0.,0.,1.),no(_Time.x*200.+37.225))*col.x;
				float3 li4 = lerp (li2,li3,smoothstep(0.5,1.,no(_Time.x*200.+78.236)));
                return float4(li4,1.);
            }
            ENDCG
        }
    }
}
