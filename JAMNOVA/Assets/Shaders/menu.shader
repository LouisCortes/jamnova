﻿Shader "Unlit/menu"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
			float hash(float t) { return frac(sin(dot(floor(t), 45.236))*4587.236); }
			float no(float t) { return lerp(hash(t), hash(t + 1.), smoothstep(0., 1., frac(t))); }
			float rd(float2 uv) { return frac(sin(dot(floor(uv), float2(45.236, 71.28)))*7845.236); }
            fixed4 frag (v2f i) : SV_Target
            {
				float c = rd(float2(0.,i.uv.y*3. + floor(_Time.x*400.)));
				float t = tex2D(_MainTex, i.uv).x;
				float t2 = tex2D(_MainTex, i.uv+float2((no(_Time.x*500.+987.004+c)-0.5)*0.05,0.)).x;
				float t3 = tex2D(_MainTex, i.uv + float2((no(_Time.x*500.+457.236+c) - 0.5)*0.05, 0.)).x;
                //float c = rd(float2(0.,i.uv.y*3.+floor(_Time.x*400.)));
				//float3 col = lerp(float3(1., 1., 1.), lerp(float3(1., 0., 0.),float3(0.,0.,1.),step(0.66,c)),step(0.33, c))*t;
				float3 col = max(float3(t, t, t),max(t2*float3(1.,0.,0.), t3*float3(0., 0., 1.)));
                return float4 (col,1.);
            }
            ENDCG
        }
    }
}
