﻿Shader "Unlit/depth"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_d1 ("d1",Range (0.,100.)) =1.
		_d2 ("d2",Range (0.,100.)) =1.
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 posWorld : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _d1;
			float _d2;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
               float c1 = smoothstep(_d1,_d2,distance(i.posWorld.rgb,_WorldSpaceCameraPos));
			  // float3 c2 = lerp(float3(1.,0.,0.),float3(0.,0.,1.),c1);
                float3 em = float3(c1,c1,c1);
                return  float4(em,1.);
            }
            ENDCG
        }
    }
}
