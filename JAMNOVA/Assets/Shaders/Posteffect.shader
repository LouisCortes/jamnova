﻿Shader "Unlit/Posteffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_f1 ("_f1",Range (0.,1.)) =1
		_f2 ("_f2",Range (0.,1.)) =1
		_f3 ("_f3",Range (0.,1.)) =1
		_f4 ("_f4",Range (0.,1.)) =1
		_f5 ("_f5",Range (0.,1.)) =1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _f1;
			float _f2;
			float _f3;
			float _f4;
			float _f5;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float2 uv2 = i.uv;
				float2 uv3 =uv2;
				float2 tuv =  (i.uv - .5);
				float2 dtuv = tuv* _f3*0.1;
				uv2 += dtuv;
				float4 col = smoothstep(0.,1., tex2D(_MainTex, uv2));
				float weight = _f2;
				for(float i=0.; i < 24.; i++){   
				uv2 -= dtuv;
				col +=  smoothstep(0.,1., tex2D(_MainTex, uv2)) * weight;
				weight *= _f1;      
				}

                //fixed4 col = float4(i.uv,0.,1.);
				
				
                return lerp(tex2D(_MainTex, uv3),col,max(step(_f4,distance(uv3.x,0.5)),step(_f5,distance(uv3.y,0.5))));
				
            }
            ENDCG
        }
    }
}
