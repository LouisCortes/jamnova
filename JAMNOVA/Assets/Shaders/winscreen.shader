﻿Shader "Unlit/winscreen"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_win ("win", 2D) = "white" {}
		_a ("activation",Range(0.,1.))=1
		[MaterialToggle]_j2("j2Win",Float) = 0
    }
    SubShader
    {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _win;
            float4 _win_ST;
			float _a;
			bool _j2;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
			float hash (float t){return frac(sin(dot(floor(t),45.236))*4587.236);}
            fixed4 frag (v2f i) : SV_Target
            {
                 float2 uv = i.uv;
			float2 ndcPos = uv * 2.0 - 1.0;

			float aspect = _ScreenParams.x / _ScreenParams.y;
			float an = 1.;
			float half_dist = tan(an);

			float2  vp_scale = float2(aspect*1.5, 1.);
			float2  P = ndcPos * vp_scale;     
			float vp_dia   = length(vp_scale);
			float rel_dist = length(P) / vp_dia;  
			float2  rel_P = normalize(P) / normalize(vp_scale);
			float2 pos_prj = ndcPos;
   
				float beta = rel_dist *an;
				pos_prj = rel_P * tan(beta) / half_dist;  

			float2 uv_prj = pos_prj * 0.5 + 0.5;
			float2 m = float2(hash(_Time.x*300.+548.23)-0.5,hash(_Time.x*300.+977.21)-0.5)*0.4;
                //fixed4 col = clamp(tex2D(_MainTex,i.uv+m*_a),float4(0.,0.,0.,0.),float4(1.,1.,1.,1.));
				float3 col1 = lerp(float3(1.,1.,1.),float3(1.,1.,1.)*step(0.5,frac(_a*100.)),step(0.2,_a)*step(_a,0.25));
				float2 col2 = tex2D(_win,i.uv+m*0.1).xy;
				float j = lerp(col2.x,col2.y,_j2);
				float3 col3 = lerp(lerp(lerp(float3(1.,0.,0.),float3(0.,0.,1.),step(0.5,hash(_Time.x*300.))),float3(1.,1.,1.),step(0.8,hash(_Time.x*200.+587.23)))*j,1.-j,step(0.8,frac(_Time.x*100.)));
                return float4(lerp(col1,col3,step(distance(pos_prj .y,0.),(_a+(hash(_Time*200.+564.23)+0.5)*0.1-0.4)*2.)),0.);
            }
            ENDCG
        }
    }
	 Fallback "Transparent/VertexLit"
}
