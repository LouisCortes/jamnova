﻿Shader "Unlit/clignotement"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_gris ("gris",Range (0.,1.)) =1.
		_hue1 ("hue1", Range(-1, 1)) = 0
        _sat1 ("saturation1", Range(-1, 1)) = 0
        _val1 ("value1", Range(-1, 1)) = 0
		_hue2 ("hue2", Range(-1, 1)) = 0
        _sat2 ("saturation2", Range(-1, 1)) = 0
        _val2 ("value2", Range(-1, 1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _gris;
			float _hue1;
			float _sat1 ;
			float _val1;
			float _hue2;
			float _sat2 ;
			float _val2;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldPos = mul (unity_ObjectToWorld, v.vertex);
                return o;
            }
			float rd (float2 uv){ return frac(sin(dot(floor(uv),float2(61.316,78.236)))*4987.236+_Time.x*100.);}
			float hash (float t){return frac(sin(dot(floor(t),45.236))*4587.236);}
			float3 hsv (float3 col, float hue, float val, float sat) {
			float4 k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 p = lerp(float4(float4(col,0.0).zy, k.wz), float4(float4(col,0.0).yz, k.xy), step(float4(col,0.0).z, float4(col,0.0).y));
                float4 q = lerp(float4(p.xyw, float4(col,0.0).x), float4(float4(col,0.0).x, p.yzx), step(p.x, float4(col,0.0).x));
                float d = q.x - min(q.w, q.y);
                float e = 1.0e-10;
                float3 f = float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);;
                return (lerp(float3(1,1,1),saturate(3.0*abs(1.0-2.0*frac((f.r+hue)+float3(0.0,-1.0/3.0,1.0/3.0)))-1),(f.g+sat))*(f.b+val));
			}
            fixed4 frag (v2f i) : SV_Target
            {

                //fixed4 col = tex2D(_MainTex, i.uv);
				
				float3 co = lerp(hsv(float3(1.,0.,0.),_hue1,_sat1,_val1),hsv(float3(0.,0.,1.),_hue2,_sat2,_val2),step(0.5,rd( i.worldPos.xz+float2(0.5,0.5))));
				//float3 co2 = lerp(co,float3(_gris,_gris,_gris),step(0.5,hash(_Time.x*300.)));
                return float4 (co,1.);
            }
            ENDCG
        }
    }
}
