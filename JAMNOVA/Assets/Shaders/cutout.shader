﻿Shader "Shader Forge/cutout" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_win ("win", 2D) = "white" {}
		_a ("activation",Range(0.,1.))=1
		[MaterialToggle]_j2("j2Win",Float) = 0
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
			sampler2D _win;
            float4 _win_ST;
			float _a;
			bool _j2;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
			float hash (float t){return frac(sin(dot(floor(t),45.236))*4587.236);}
            float4 frag(VertexOutput i) : COLOR {
			     float2 uv = i.uv0;
			float2 ndcPos = uv * 2.0 - 1.0;

			float aspect = _ScreenParams.x / _ScreenParams.y;
			float an = 1.;
			float half_dist = tan(an);

			float2  vp_scale = float2(aspect*1.5, 1.);
			float2  P = ndcPos * vp_scale;     
			float vp_dia   = length(vp_scale);
			float rel_dist = length(P) / vp_dia;  
			float2  rel_P = normalize(P) / normalize(vp_scale);
			float2 pos_prj = ndcPos;
   
				float beta = rel_dist *an;
				pos_prj = rel_P * tan(beta) / half_dist;  

			float2 uv_prj = pos_prj * 0.5 + 0.5;
                clip(step(distance(pos_prj .y,0.),(_a+(hash(_Time*200.+564.23)+0.5)*0.1-0.4)*2.)- 0.5);
////// Lighting:
////// Emissive:
                float2 m = float2(hash(_Time.x*300.+548.23)-0.5,hash(_Time.x*300.+977.21)-0.5)*0.4;
                //fixed4 col = clamp(tex2D(_MainTex,i.uv+m*_a),float4(0.,0.,0.,0.),float4(1.,1.,1.,1.));
				float3 col1 = lerp(float3(1.,1.,1.),float3(1.,1.,1.)*step(0.5,frac(_a*100.)),step(0.2,_a)*step(_a,0.25));
				float2 col2 = tex2D(_win,i.uv0+m*0.1).xy;
				float j = lerp(col2.x,col2.y,_j2);
				float3 col3 = lerp(lerp(lerp(float3(1.,0.,0.),float3(0.,0.,1.),step(0.5,hash(_Time.x*300.))),float3(1.,1.,1.),step(0.8,hash(_Time.x*200.+587.23)))*j,1.-j,step(0.8,frac(_Time.x*100.)));
                return float4(lerp(col1,col3,step(distance(pos_prj .y,0.),(_a+(hash(_Time*200.+564.23)+0.5)*0.1-0.4)*2.)),1.);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                clip(i.uv0.x - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}