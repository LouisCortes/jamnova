﻿Shader "Unlit/jeux02"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_t1 ("redcam", 2D) = "white" {}
		_t2 ("bluecam", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _t1;
            float4 _t1_ST;
			sampler2D _t2; 

            float4 _t2_ST;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
                //fixed4 col = tex2D(_MainTex, i.uv);
				float3 col =tex2D(_t1, i.uv).xyz*tex2D(_t2, i.uv).xyz;
                return float4 (col,1.);
            }
            ENDCG
        }
    }
}
