﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitStairs : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Player")){
            Debug.Log("youhou");
            if (SceneManager.GetActiveScene().name == "PlanNova1")
            {
                LoadLevel("PlanNova2");
            }
            else if (SceneManager.GetActiveScene().name == "PlanNova2" || SceneManager.GetActiveScene().name == "PlanNova2Start")
            {
                LoadLevel("PlanNova1");
            }


        }
    }

    private void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
