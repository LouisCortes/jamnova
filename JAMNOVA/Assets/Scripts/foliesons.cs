﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class foliesons : MonoBehaviour
{
   public  AudioSource sons;
    float frac(float s) { return s - Mathf.Floor(s); }
    float rand(float s) { return frac(Mathf.Sin(Vector2.Dot(new Vector2(Mathf.Floor(s), 0.0f), new Vector2(12.654f, 0.0f))) * 4032.326f); }
    float noi(float s) { return Mathf.Lerp(rand(s), rand(s + 1.0f), Mathf.SmoothStep(0.25f, 7.5f, frac(s))); }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float theUnityTime = Time.time;
        sons.pitch =noi(theUnityTime*2.0f)+1.0f;
        sons.panStereo =( rand(theUnityTime * 4.0f)-0.5f)*2.0f;
    }
}
