﻿using UnityEngine;
using UnityEngine.UI;

public class barInteraction : MonoBehaviour
{
    public TextAsset textFile;
    public string[] textLines;
     
    public GameObject textBox;
    public Text theText;

    public int currentLine;
    public int EndAtLine;
    
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Player")){
            textBox.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if(collider.CompareTag("Player")){
            textBox.SetActive(false);
            currentLine = 0;
        }
    }

    void Start()
    {
        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }

        if (EndAtLine == 0)
        {
            EndAtLine = textLines.Length - 1;
        }
    }

    private void Update()
    {
        if (textBox.activeSelf == true)
        {
            theText.text = textLines[currentLine];
            if (Input.anyKeyDown && Input.GetAxis("Horizontal")==0.0f && Input.GetAxis("Vertical")==0.0f)
            {
                currentLine += 1;
            }

            if (currentLine > EndAtLine)
            {
                textBox.SetActive(false);
                currentLine = 0;
            }
        }

    }
}
