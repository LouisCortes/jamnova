﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Cine : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.CompareTag("Player")){
            Debug.Log("bouuhou");
            LoadLevel("SCN_Game01");
        }
    }

    private void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
