﻿using UnityEngine;

public class ControllerMove : MonoBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 2.0f;
    public Material animationMat;
    
    
    // Start is called before the first frame update
    void Start()
    {
        animationMat.SetFloat("_animation", 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        float translationX = Input.GetAxis("Horizontal") * speed;
        float translationZ = Input.GetAxis("Vertical") * speed;
        if (translationX == 0.0f && translationZ == 0.0f)
        {
            animationMat.SetFloat("_animation", 0.0f);
        } else 
        {
            animationMat.SetFloat("_animation", 1.0f);
        }

        translationX *= Time.deltaTime;
        translationZ*= Time.deltaTime;
        transform.Translate(translationX,0,translationZ);
        
        //float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        //rotation *= Time.deltaTime;
        //transform.Rotate(0, rotation, 0);
    }

}
