﻿using UnityEngine;

public class ControllerMove2 : MonoBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 2.0f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float translationX = Input.GetAxis("Horizontal2") * speed;
        float translationZ = Input.GetAxis("Vertical2") * speed;
        translationX *= Time.deltaTime;
        translationZ*= Time.deltaTime;
        transform.Translate(translationX,0,-translationZ);
        
        //float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        //rotation *= Time.deltaTime;
        //transform.Rotate(0, rotation, 0);
    }
}
